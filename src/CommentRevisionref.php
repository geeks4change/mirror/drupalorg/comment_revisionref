<?php

namespace Drupal\comment_revisionref;

class CommentRevisionref {

  /**
   * Get applicable comment-to-host revision reference field name.
   *
   * A comment type can apply to multiple bundles of a single entity type.
   * So to better guess which is the right field, we check host bundle too.
   *
   * @param $commentBundleId
   *   The comment bundle ID.
   * @param $hostBundleId
   *   The host bundle ID.
   *
   * @return string|null
   *   The field name, or NULL if none is found.
   *
   * @throws \Drupal\Component\Plugin\Exception\InvalidPluginDefinitionException
   * @throws \Drupal\Component\Plugin\Exception\PluginNotFoundException
   */
  public static function applicableFieldName($commentBundleId, $hostBundleId) {
    /** @var \Drupal\Core\Entity\EntityFieldManagerInterface $entityFieldManager */
    $entityFieldManager = \Drupal::service('entity_field.manager');

    $entityTypeManager = \Drupal::entityTypeManager();

    $commentEntityType = $entityTypeManager->getDefinition('comment');
    $commentBundleStorage = $entityTypeManager->getStorage($commentEntityType->getBundleEntityType());

    /** @var \Drupal\comment\CommentTypeInterface $commentType */
    $commentType = $commentBundleStorage->load($commentBundleId);
    $hostEntityTypeId = $commentType->getTargetEntityTypeId();

    $fieldDefinitions = $entityFieldManager->getFieldDefinitions('comment', $commentBundleId);
    $applicableFieldName = NULL;
    foreach ($fieldDefinitions as $fieldName => $fieldDefinition) {
      if (
        $fieldDefinition->getType() === 'entity_reference_revisions'
        && $fieldDefinition->getSetting('target_type') === $hostEntityTypeId
        && ($handlerSettings = $fieldDefinition->getSetting('handler_settings'))
        && !empty($handlerSettings['target_bundles'])
        && array_keys($handlerSettings['target_bundles']) === [$hostBundleId]
      ) {
        $applicableFieldName = $fieldName;
        break;
      }
    }
    return $applicableFieldName;
  }

}
